/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EjemploIngresoTeclado;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *
 * @author heltonsmith
 */
public class Principal {
    public static void main(String[] args) throws Exception{
        
        InputStreamReader st = new InputStreamReader(System.in);
        BufferedReader bf = new BufferedReader(st);
        
        System.out.println("Ingrese num1: ");
        String num1 = bf.readLine();
        
        System.out.println("Ingrese num2: ");
        String num2 = bf.readLine();
        
        
        int n1 = Integer.parseInt(num1);
        int n2 = Integer.parseInt(num2);
        
        int resultado = n1+n2;
        
        
        System.out.println("Resultado: " + resultado);
        
        
    }
}
